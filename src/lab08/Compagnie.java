/**
 * 
 */
package lab08;

/**
 * @author Johnny Tsheke @ UQAM --INF1256 
 *
 */
import lab08.Employe;
public class Compagnie {

	/**
	 * Programme principal
	 */
	public static void main(String[] args) {
	
		System.out.println("Index classe Employe = "+ Employe.getIndex());
		Employe employe1 = new Employe("Vincent", "Gagnon");
		Employe employe2 = new Employe("Solange", "Matonge");
        employe1.displayInfo();
        employe2.displayInfo();
        System.out.println("Index classe Employe vaut maintenat = "+ Employe.getIndex());
	}

}
