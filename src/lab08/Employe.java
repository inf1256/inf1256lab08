/**
 * 
 */
package lab08;

/**
 * @author Johnny Tsheke @ UQAM -- INF1256 --familiarisation O.O.
 *
 */
class Employe {
             private static int indexAutomatique = 1;
             private int matricule = 0;
             private int anciennete = 0;
             private String nom;
             private String prenom;
             
             public Employe(String fn, String ln){//first name, last name
            	 prenom = fn;
            	 nom = ln;
            	 matricule = indexAutomatique;
            	 indexAutomatique++;
            	 anciennete++;
             }
             
             public static int getIndex(){
            	 return(indexAutomatique);
             }
             
             public int getMatricule(){
            	 return(matricule);
             }
             
             protected void displayInfo(){
            	 System.out.println("   -- Info employé --    ");
            	 System.out.println("Nom : "+nom);
            	 System.out.println("Prénom : "+prenom);
            	 System.out.println("Matricule : "+matricule);
            	 System.out.println("Ancienneté : "+anciennete);
            	 System.out.println("   ---    ");//juste pour indiquer la fin info employe
            	 
             }
}
